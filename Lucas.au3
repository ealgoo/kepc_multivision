#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.8.1
 Author: Ealgoo Kim

 Script Function:
	Show Lucas Video in Loop with certain time period at certain place in the Multivision.

#ce ----------------------------------------------------------------------------


; Show Website
Run("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe http://kepc.org");
;WinWait( "Google Chrome");
Sleep(1000);
WinMove( "", "", 0,0, 1366, 768 );

; Show Movies
While 1
Run("cd C:\Users\Frys Electronics\Desktop\140406\Multivision");
Run("C:\Program Files (x86)\GRETECH\GomPlayer\GOM.EXE LUCAS-01.avi");
WinWait( "LUCAS-01");
WinMove( "LUCAS-01", "", 0,0, 1366, 768);
Sleep(144000);
Winclose( "LUCAS-01" );
Sleep(30000);
Run("cd C:\Users\Frys Electronics\Desktop\140406\Multivision");
Run("C:\Program Files (x86)\GRETECH\GomPlayer\GOM.EXE LUCAS-02.avi");
WinWait( "LUCAS-02");
WinMove( "LUCAS-02", "", 0,0, 1366, 768);
Sleep(78000);
Winclose( "LUCAS-02" );
Sleep(30000);
Run("cd C:\Users\Frys Electronics\Desktop\140406\Multivision");
Run("C:\Program Files (x86)\GRETECH\GomPlayer\GOM.EXE LUCAS-03.avi");
WinWait( "LUCAS-03");
WinMove( "LUCAS-03", "", 0,0, 1366, 768);
Sleep(111000);
Winclose( "LUCAS-03" );
Sleep(30000);
WEnd
